#!/bin/bash

# Path to Selenium binary

# Working dir, usually ./app/iquall/IquallPrimitives/youtube
WORKING_DIR="/tmp"
# Path to node binary
# Use Virtual X
USE_VIRTUAL_X=true
# Log file path, either relative to WORKING_DIR, or absolute.
LOG_FILE_PATH="/home/iquall/sonar-broadband/script_simulator.log"

#Parametros que recibe  el script de python
#   sonda.unno.9 7
SCRIPT_PYTHON=$1
URL=$2
SCRIPT_NAME=$3



# Overwrite with custom settings
FILE="./internal-config/selenium.ini" && test -f "$FILE" && source "$FILE"

cd "$WORKING_DIR" || ( echo "{ 'error' : 'Unable to change dir to $working_dir', 'PWD' : '$PWD'}"   && exit 1 );

cd /tmp && rm -r .com.google.Chrome*

function killAll() {
	if [ true ]
	then
		killall java 1>> "$LOG_FILE_PATH" 2>> "$LOG_FILE_PATH"
		killall chromedriver 1>> "$LOG_FILE_PATH" 2>> "$LOG_FILE_PATH"
		if [ $USE_VIRTUAL_X = true ]
		then
			killall chrome 1>> "$LOG_FILE_PATH" 2>> "$LOG_FILE_PATH"
			killall Xvfb 1>> "$LOG_FILE_PATH" 2>> "$LOG_FILE_PATH"
		fi
	fi
}
FILE_PYTHON=$WORKING_DIR$SCRIPT_NAME
echo  $SCRIPT_PYTHON | base64 -d > $FILE_PYTHON

if [ $USE_VIRTUAL_X = true ]
then
	export DISPLAY=:15
fi

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.

killAll

if [ $USE_VIRTUAL_X = true ]
then
	Xvfb :15 -screen 0 1920x1080x24 -ac 1>> "$LOG_FILE_PATH" 2>> "$LOG_FILE_PATH" &
fi

python $FILE_PYTHON $URL

killAll

exit 0
